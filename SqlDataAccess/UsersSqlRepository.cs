﻿using SharedModels.Interfaces;
using SharedModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    public class UsersSqlRepository : IUserRepository
    {
        private readonly ISqlDataAccess _dbase;
        private readonly Dictionary<string, string> _userColorMap;

        public UsersSqlRepository(ISqlDataAccess dbase)
        {
            _dbase = dbase;
            _userColorMap = new Dictionary<string, string>();

            string sqlQuery = $"SELECT * FROM dbo.ChatUsers";

            List<ChatUser> users = _dbase.LoadData<ChatUser, dynamic>(sqlQuery, new { });

            foreach (var user in users)
            {
                _userColorMap[user.UserNickname] = user.HtmlColor;
            }
        }


        //public string GetUserColor(string nickname)
        //{
        //    // See https://www.learndapper.com/parameters 

        //    string sqlQuery = $"SELECT * FROM dbo.ChatUsers WHERE UserNickname = @Nickname";

        //    ChatUser user = _dbase.LoadSingle<ChatUser, dynamic>(sqlQuery, new { Nickname = nickname });

        //    return user.HtmlColor;
        //}

        public string GetUserColor(string nickname)
        {
            try
            {
                return _userColorMap[nickname];
            }
            catch (Exception)
            {
                throw new ApplicationException($"User {nickname} does not exist in the database.");
            }
        }
    }
}
