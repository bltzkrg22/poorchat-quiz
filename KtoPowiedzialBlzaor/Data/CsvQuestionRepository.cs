﻿using Microsoft.Extensions.Configuration;
using SharedModels;
using SharedModels.Interfaces;
using SharedModels.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KtoPowiedzialBlzaor.Data
{
    class CsvQuestionRepository : IQuestionRepository
    {
        private readonly IConfiguration _configData;
        private readonly Dictionary<uint, Question> _questions;


        public Question GetQuestion(uint id)
            => _questions[id];

        public Question GetRandomQuestion()
            => _questions.ElementAt(Rng.GetRandom(_questions.Count)).Value;

        public List<uint> GetQuestionIds()
            => _questions.Keys.ToList();

        public int Count => _questions.Count;


        // Constructor is too busy, but I don’t care enough to offload this to another method
        public CsvQuestionRepository(IConfiguration configData)
        {
            _configData = configData;

            _questions = new Dictionary<uint, Question>();

            string questionsRelativePath = _configData.GetValue<string>("CsvQuestionRepositoryPath");
            string questionsFullPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), questionsRelativePath));

            StreamReader fileReader = new StreamReader(questionsFullPath);
            string csvLine;
            while ((csvLine = fileReader.ReadLine()) is not null)
            {
                string[] fields = csvLine.Split(',');
                uint id = UInt32.Parse(fields[0]);
                string questionText = fields[1];
                string correctUserNickname = fields[2];
                string wrongUserNicknameA = fields[3];
                string wrongUserNicknameB = fields[4];

                if (_questions.ContainsKey(id))
                {
                    throw new ApplicationException($"Multiple definitions in file {questionsRelativePath} for question id {id}");
                }
                else
                {
                    _questions[id] = new Question()
                    {
                        Id = id,
                        QuestionText = questionText,
                        CorrectUserNickname = correctUserNickname,
                        WrongUserNicknameA = wrongUserNicknameA,
                        WrongUserNicknameB = wrongUserNicknameB
                    };
                }
            }
        }
    }
}
