﻿using Microsoft.Extensions.Configuration;
using SharedModels.Interfaces;
using SharedModels.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace KtoPowiedzialBlzaor.Data
{
    public class CsvUserRepository : IUserRepository
    {
        private readonly IConfiguration _configData;
        private Dictionary<string, ChatUser> _users;
        public string GetUserColor(string nickname)
        {
            if (_users.ContainsKey(nickname))
            {
                return _users[nickname].HtmlColor;
            }
            else
            {
                throw new ApplicationException($"User with name {nickname} is not defined in the database");
            }
        }

        // Constructor is too busy, but I don’t care enough to offload this to another method
        public CsvUserRepository(IConfiguration configData)
        {
            _configData = configData;

            _users = new Dictionary<string, ChatUser>();

            string usersRelativePath = _configData.GetValue<string>("CsvUserRepositoryPath");
            string usersFullPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), usersRelativePath));

            StreamReader fileReader = new StreamReader(usersFullPath);
            string csvLine;
            while ((csvLine = fileReader.ReadLine()) is not null)
            {
                string[] fields = csvLine.Split(',');
                string userNickname = fields[0];
                string htmlColor = fields[1];

                //var newUser = new ChatUser() { UserNickname = fields[0], HtmlColor = fields[1] };

                if (_users.ContainsKey(userNickname))
                {
                    throw new ApplicationException($"Multiple definitions in file {usersRelativePath} for user {userNickname}");
                }
                else
                {
                    _users[userNickname] = new ChatUser() { UserNickname = userNickname, HtmlColor = htmlColor };
                }
            }
        }
    }
}
