# Poorchat Quiz

<small>**Attention**</small>

<small>The SQL database is not free to host, so after the successful presentation I shut it down, and enabled the backup, hardcoded question repository, which was not in sync with the SQL database.</small>

<small>If I find some time, I’ll update the app and restore the questions.</small>

-----

A very basic web app that demonstrates the usage of:

* Blazor Server – the main application framework,
* Dapper – access to SQL database.

The SQL access layer is “decoupled” from the UI and implemented as a separate class library, `SqlDataAccess`. To reduce code duplication, data models shared by UI and data library are specified in additional shared lib: `SharedModels`.

## How it works

The quiz database is composed of two tables:

1. Questions,
2. ChatUsers.

When the quiz is started, application loads (only) the id numbers of all questions from the database, and shuffles them into a randomly ordered list. Afterwards full questions are read one by one, and the user may try to guess the correct answer – the nickname of person whose quote is displayed.

The second table is used only to associate a color parameter with the nickname.

There is no in-app interface to add new questions/users to the database – they were created manually through simple SQL queries.

## Additional information

The application is deployed to Azure Web Services, and makes use of the Azure SQL Database.
