﻿using SharedModels.Models;
using System.Collections.Generic;

namespace SharedModels.Interfaces
{
    public interface IQuestionRepository
    {
        public int Count { get; }
        public Question GetRandomQuestion();
        public Question GetQuestion(uint id);
        public List<uint> GetQuestionIds();
    }
}
